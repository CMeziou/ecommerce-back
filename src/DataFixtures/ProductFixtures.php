<?php

namespace App\DataFixtures;


use App\Entity\Brand;
use App\Entity\Comment;
use App\Entity\Image;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;


class ProductFixtures extends Fixture 
{
    public const PRODUCT_REFERENCE = 'product';
   
  
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i=0; $i < 10; $i++) { 
            
            $brand = new Brand();
            $brand->setLabel($faker->company());
            $manager->persist($brand);
           

            for ($j=0; $j < 10; $j++) { 
                
                $product = new Product();
                $product->setTitle($faker->word());
                $product->setDescription($faker->paragraph(2));
                $product->setPrice($faker->randomFloat(2,1,160));
                $product->setStocks($faker->numberBetween(0,15));
                $product->setBrand($brand);
                $product->addCategory($this->getReference(CategoryFixtures::CATEGORY_REFERENCE.$faker->numberBetween(0,3)));
                
                $manager->persist($product);
                $this->addReference(self::PRODUCT_REFERENCE.$i.'-'.$j, $product);
               
            
            
                
                for ($k=0; $k < $faker->numberBetween(2,4); $k++) { 
                    $image = new Image();
                    $image->setLink($faker->imageUrl());
                    $image->setProduct($product);
                    $manager->persist($image);
                }
                
            }


        }

        $manager->flush();
    }
   
    
}
