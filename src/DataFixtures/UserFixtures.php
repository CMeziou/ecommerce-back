<?php

namespace App\DataFixtures;

use App\Entity\Adress;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    public const USER_REFERENCE = 'user';
    public const ADRESS_REFERENCE = 'adress';
   
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();


        for ($v = 0; $v < 10; $v++) {

            $user = new User();
            $user->setName($faker->name());
            $user->setLastname($faker->lastName());
            $user->setMail($faker->email());
            $user->setPhone($faker->phoneNumber());
            $user->setPassword($faker->password());
            $user->addProduct($this->getReference(ProductFixtures::PRODUCT_REFERENCE.$faker->numberBetween(0,9).'-'.$faker->numberBetween(0,9)));
            $user->setRole($faker->userName);
            $manager->persist($user);
            $this->addReference(self::USER_REFERENCE.$v, $user);
        


            for ($w = 0; $w < 3; $w++) {

                $adress = new Adress();
                $adress->setStreetNum($faker->numberBetween(1, 200));
                $adress->setStreet($faker->streetName());
                $adress->setZipCode($faker->numerify()); 
                $adress->setCity($faker->city());
                $adress->setUser($user);
                $manager->persist($adress);
                $this->addReference(self::ADRESS_REFERENCE.$v.'-'.$w, $adress);
               
            }


        }

        $manager->flush();
    }
	/**
	 * This method must return an array of fixtures classes
	 * on which the implementing class depends on
	 * @return array<string>
	 */
	public function getDependencies() {
        return [
            ProductFixtures::class,

        ];
	}

}