<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker\Factory;


class CommentFixtures extends Fixture implements DependentFixtureInterface
{

    

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($x=0; $x < 10; $x++) { 
            
                    $comment = new Comment();
                    $comment->setRate($faker->numberBetween(0,5));
                    $comment->setAuthor($faker->userName);
                    $comment->setDate($faker->dateTime());
                    $comment->setContent($faker->paragraph(2));
                    $comment->setImage($faker->imageUrl());
                    $comment->setUser($this->getReference(UserFixtures::USER_REFERENCE.$faker->numberBetween(0,9)));
                    $comment->setProduct($this->getReference(ProductFixtures::PRODUCT_REFERENCE.$faker->numberBetween(0,9).'-'.$faker->numberBetween(0,9)));

                    $manager->persist($comment);
                   
                }
            
        

        $manager->flush();
    }
    public function getDependencies() {
        return [
            ProductFixtures::class,
            UserFixtures::class,
        ];
	}
    
}
