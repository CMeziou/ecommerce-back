<?php

namespace App\DataFixtures;


use App\Entity\LineProduct;
use App\Entity\Order;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class OrderFixtures extends Fixture implements DependentFixtureInterface
{
    public const ORDER_REFERENCE = 'order';
   
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();


        for ($s = 0; $s < 4; $s++) {

            $order = new Order();
            $order->setTitle($faker->word());
            $order->setUser($this->getReference(UserFixtures::USER_REFERENCE.$faker->numberBetween(0,9)));
            $order->setDetail($faker->sentence());
            $order->setAdress($this->getReference(UserFixtures::ADRESS_REFERENCE.$faker->numberBetween(0,9).'-'.$faker->numberBetween(0,2)));
            $order->setTotal($faker->randomFloat(2,1,160));
            $order->setStatus($faker->word());
           
            $manager->persist($order);
       

            for ($t = 0; $t < 3; $t++) {

                $lineProduct = new LineProduct();
                $lineProduct->setTitle($faker->word());
                $lineProduct->setQuantity($faker->numberBetween(1,6));
                $lineProduct->setCustom($faker->boolean());
                $lineProduct->setPrice($faker->randomFloat(2,1,160));
                $lineProduct->setProduct($this->getReference(ProductFixtures::PRODUCT_REFERENCE.$faker->numberBetween(0,9).'-'.$faker->numberBetween(0,9)));
                $lineProduct->setProductOrder($order);
                $manager->persist($lineProduct);
            }


        }

        $manager->flush();
    }
	/**
	 * This method must return an array of fixtures classes
	 * on which the implementing class depends on
	 * @return array<string>
	 */
	public function getDependencies() {
        return [
            UserFixtures::class,
            ProductFixtures::class

        ];
	}

}