<?php

namespace App\Controller;

use App\Entity\Order;
use App\Repository\OrderRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route('/api/order')]
class OrderController extends AbstractController
{
    private OrderRepository $repo;
    public function __construct(OrderRepository $repo) {
    	$this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function all():Response 
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $order = $this->repo->find($id);
        if(!$order){
            throw new NotFoundHttpException();
        }
        return $this->json($order);
    }

    #[Route(methods:'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        $order = $serializer->deserialize($request->getContent(), Order::class, 'json');
        $this->repo->save($order, true);
        return $this->json($order, Response::HTTP_CREATED);
    }

    #[Route("/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(Order $order, Request $request, SerializerInterface $serializer) {
        try{

            $serializer->deserialize($request->getContent(), Order::class, 'json', [
                'object_to_populate' => $order,
                'deep_object_to_populate' => true // peut être inutile
            ]);
            $this->repo->save($order, true);
            return $this->json($order);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }


    #[Route("/{id}", methods: 'DELETE')]
    public function delete(Order $order) {
        $this->repo->remove($order, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
    #[Route("/finish/{id}", methods: ['POST'])]
    public function finish(Order $order, OrderRepository $repo)
    {
        {$order && 
            $order->setStatus('en cours');
        }
        $order->setStatus('finished');
        $order->setDetail('Merci pour votre commande. Voici le détail de celle-ci');
        $order->setTitle(('Commande'));
        $order->setTotal(0);

        // $adress = new Adress();
        // $user->addAdress($adress);
        // $adress->addOrder($order);
        // $adress->setStreetNum('');
        // $adress->setStreet('test');
        // $adress->setZipCode(0);
        // $adress->setCity('');


        $repo->save($order, true);

        return $this->json($order);
    }
}
