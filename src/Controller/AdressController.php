<?php

namespace App\Controller;

use App\Entity\Adress;
use App\Repository\AdressRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;


#[Route('/api/adress')]
class AdressController extends AbstractController
{
    public function __construct(private AdressRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(Adress $adress)
    {

        return $this->json($adress);
    }

    // #[Route(methods: 'POST')]
    // public function add(Request $request, SerializerInterface $serializer)
    // {
    //     try {
    //         $adress = $serializer->deserialize($request->getContent(), Adress::class, 'json');
    //         $this->repo->save($adress, true);

    //         return $this->json($adress, Response::HTTP_CREATED);
    //     } catch (ValidationFailedException $e) {
    //         return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
    //     } catch (NotEncodableValueException $e) {
    //         return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
    //     }
    // }

    #[Route("/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(Adress $adress, Request $request, SerializerInterface $serializer)
    {
        try {

            $serializer->deserialize($request->getContent(), Adress::class, 'json', [
                'object_to_populate' => $adress
            ]);
            $this->repo->save($adress, true);
            return $this->json($adress);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }

    #[Route("/{id}", methods: 'DELETE')]
    public function delete(Adress $adress)
    {
        $this->repo->remove($adress, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

}