<?php
namespace App\Controller;

use App\Entity\Adress;
use App\Entity\LineProduct;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\User;
use App\Repository\LineProductRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route('/api/cart')]
/**
 * Summary of CartController
 */
class CartController extends AbstractController
{
    /**
     * Summary of cart
     * @param LineProduct $lineProduct
     * @return Response
     * poste une line product dans le panier (id->product)
     */
    #[Route("/{id}", methods: 'POST')]
    public function addToCart(Product $product, OrderRepository $repo, LineProductRepository $lineRepo): Response
    {
        $lineProduct = new LineProduct();

        $lineProduct->setQuantity(1);
        $panier = $repo->findOneBy(['user' => $this->getUser(), 'status' => 'cart']);
        $lineProduct->setProductOrder($panier);
        $lineProduct->setProduct($product);
        $lineProduct->setTitle($product->getTitle());
        $lineProduct->setPrice($product->getPrice());
        //save la line
        $lineRepo->save($lineProduct, true);

        return $this->json($lineProduct);

    }

    //recupere l'order/panier du user conecté et l'assigner au lineProduct
    #[Route("/{id}", methods: 'DELETE')]
    public function deleteFromcart(LineProductRepository $repo, LineProduct $lineProduct)
    {
        $repo->remove($lineProduct, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
    #[Route("/validate", methods: ['PATCH', 'PUT'])]
    public function validate(OrderRepository $repo, ProductRepository $reposit)
    {
        $panier = $repo->findOneBy(['user' => $this->getUser(), 'status' => 'cart']);
        if (!$panier->getLineProducts()->isEmpty()) {
            
            $panier->setStatus('en cours');
            foreach ($panier->getLineProducts() as $key => $line) {
                $product = $line->getProduct();
                $quantity = $line->getQuantity();
                $stock = $product->getStocks();
                $product->setStocks($stock - $quantity);
                
                $reposit->save($product);
            }
           
            $order = new Order;
            $order->setUser($this->getUser());
            $order->setStatus('cart');
            $order->setDetail('panier');
            $order->setTitle(('panier'));
            $order->setTotal(0);
            $repo->save($panier);

            $repo->save($order, true);
        
        }
        return $this->json($panier);
    

    }

    #[Route("/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(LineProduct $lineProduct, LineProductRepository $repo, Request $request, SerializerInterface $serializer)
    {
        try {

            $serializer->deserialize($request->getContent(), LineProduct::class, 'json', [
                'object_to_populate' => $lineProduct
            ]);
            $repo->save($lineProduct, true);
            return $this->json($lineProduct);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }


    #[Route('/current', methods: 'GET')]

    public function getCart(OrderRepository $repo)
    {
        $currentUser = $repo->findOneBy(['user' => $this->getUser()]);
        if (!$currentUser) {
            return $this->json(['error' => 'No Current Order'], Response::HTTP_NOT_FOUND);
        }

        return $this->json($currentUser);
    }


//     #[Route(methods: 'GET')]
//     public function orderHistory(OrderRepository $order) {
//         $orders = $order->findHistory($this->getUser());
//         // $rentals = $repo->findBy(['user' => $this->getUser()]);

//         return $this->json($orders);
//     }   
}