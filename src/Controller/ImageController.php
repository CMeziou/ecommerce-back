<?php

namespace App\Controller;

use App\Entity\Image;
use App\Repository\ImageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

#[Route('/api/image')]
class ImageController extends AbstractController
{
    private ImageRepository $repo;

    public function __construct(ImageRepository $repo) {
    	$this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function all():Response 
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $image = $this->repo->find($id);
        if(!$image){
            throw new NotFoundHttpException();
        }
        return $this->json($image);
    }

    #[Route(methods:'POST')]
    public function add(Request $request, SerializerInterface $serializer) {
        $image = $serializer->deserialize($request->getContent(), Image::class, 'json');
        $this->repo->save($image, true);
        return $this->json($image, Response::HTTP_CREATED);
    }

    #[Route("/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(Image $image, Request $request, SerializerInterface $serializer) {
        try{

            $serializer->deserialize($request->getContent(), Image::class, 'json', [
                'object_to_populate' => $image,
                'deep_object_to_populate' => true // peut être inutile
            ]);
            $this->repo->save($image, true);
            return $this->json($image);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route("/{id}", methods: 'DELETE')]
    public function delete(Image $image)
    {
        $this->repo->remove($image, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}
