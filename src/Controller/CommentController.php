<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Image;
use App\Entity\Product;
use App\Service\Uploader;
use Intervention\Image\ImageManager;
use App\Repository\CommentRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;


#[Route('/api/comment')]
class CommentController extends AbstractController
{
    public function __construct(private CommentRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(Comment $comment)
    {

        return $this->json($comment);
    }

    #[Route('/{id}', methods: 'POST')]
    public function addComment(Product $product, Request $request, SerializerInterface $serializer, Uploader $uploader)
    {
        try {
            $comment = $serializer->deserialize($request->getContent(), Comment::class, 'json');
            $comment->setUser($this->getUser());
            if(!empty($comment->getImage())) {
                $comment->setImage($uploader->upload($comment->getImage()));
            }
    
            $comment->setProduct($product);
            $comment->setDate(new \DateTime);
            $this->repo->save($comment, true);

            return $this->json($comment, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route("/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(Comment $comment, Request $request, SerializerInterface $serializer)
    {
        try {

            $serializer->deserialize($request->getContent(), Comment::class, 'json', [
                'object_to_populate' => $comment
            ]);
            $this->repo->save($comment, true);
            return $this->json($comment);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }

    #[Route("/{id}", methods: 'DELETE')]
    public function delete(Comment $comment)
    {
        $this->repo->remove($comment, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

}
