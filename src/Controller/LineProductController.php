<?php

namespace App\Controller;

use App\Entity\LineProduct;
use App\Repository\LineProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

#[Route('/api/lineproduct')]
class LineProductController extends AbstractController
{
    public function __construct(private LineProductRepository $repo) {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(LineProduct $lineProduct)
    {

        return $this->json($lineProduct);
    }
    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $lineProduct = $serializer->deserialize($request->getContent(), LineProduct::class, 'json');
            $this->repo->save($lineProduct, true);

            return $this->json($lineProduct, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

        
    }


    #[Route("/{id}", methods: 'DELETE')]
    public function delete(LineProduct $lineProduct)
    {
        $this->repo->remove($lineProduct, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }


    #[Route("/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(LineProduct $lineProduct, Request $request, SerializerInterface $serializer)
    {
        try {

            $serializer->deserialize($request->getContent(), LineProduct::class, 'json', [
                'object_to_populate' => $lineProduct
            ]);
            $this->repo->save($lineProduct, true);
            return $this->json($lineProduct);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }
}
