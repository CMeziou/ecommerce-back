<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/user')]
class UserController extends AbstractController
{

    public function __construct(private UserRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(User $user)
    {

        return $this->json($user);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $user = $serializer->deserialize($request->getContent(), User::class, 'json');
            $this->repo->save($user, true);

            return $this->json($user, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
        
    }


    #[Route("/{id}", methods: 'DELETE')]
    public function delete(User $user)
    {
        $this->repo->remove($user, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }


    #[Route("/{id}", methods: ['PATCH', 'PUT'])]
    public function patch(User $user, Request $request, SerializerInterface $serializer)
    {
        try {

            $serializer->deserialize($request->getContent(), User::class, 'json', [
                'object_to_populate' => $user
            ]);
            $this->repo->save($user, true);
            return $this->json($user);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }

   

}
