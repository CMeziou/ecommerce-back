### Ecommerce Back - Laure, Yann, Chems, Samantha

## Organisation

Le projet Ecommerce Back a été organisé de manière collaborative en suivant les étapes ci-dessous :

- Mise en place d'un git et d'un board avec des issues pour une gestion efficace des tâches.
- Utilisation de Fixtures avec un système de boucles et de références, en cohérence avec le diagramme de classe, pour remplir la base de données.
- Réunions régulières en début de session pour la planification et la coordination.
- Chaque membre a travaillé sur au moins une partie de chaque élément pour promouvoir l'apprentissage mutuel.
- Livecoding et explications pour garantir une compréhension commune du code.
- Utilisation de Doctrine pour créer les entités, leurs repositories, et établir les relations nécessaires.
- Création des contrôleurs, de méthodes personnalisées, et des routes correspondantes.
- Tests des routes avec ThunderClient et jeu de données spécifique.
- Ajout d'Ignores sur certains éléments pour éviter les références circulaires.
- Mise en place de l'authentification des utilisateurs avec un système de token JWT.
- Restriction d'accès sur certaines routes.
- Création d'un service Uploader pour le téléchargement d'images avec un dossier "uploads" dans le répertoire public.


## Outils Utilisés

- StarUML pour les diagrammes de classes et les User Cases.
- Figma pour les wireframes.
- MariaDB pour la base de données MySQL.
- Symfony avec Doctrine pour le backend.
- React pour le frontend.
- Gitlab pour la gestion du code source.

## Améliorations Envisagées

- Ajout de méthodes personnalisées ou modifications de certains contrôleurs en lien avec les fonctionnalités du site (gestion du stock lors de la validation d'une commande, gestion des duplicatas dans le panier, etc.).
- Remplacement des fixtures par de vraies données dans la base de données.
- Ajout de tests pour le backend.
